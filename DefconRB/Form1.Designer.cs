﻿namespace DefconRB
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHostname = new System.Windows.Forms.Label();
            this.lblWan = new System.Windows.Forms.Label();
            this.txtHostname = new System.Windows.Forms.TextBox();
            this.txtWanIP = new System.Windows.Forms.TextBox();
            this.txtConfig = new System.Windows.Forms.TextBox();
            this.txtWanMask = new System.Windows.Forms.TextBox();
            this.txtDNS2 = new System.Windows.Forms.TextBox();
            this.txtDNS1 = new System.Windows.Forms.TextBox();
            this.txtTsgMask = new System.Windows.Forms.TextBox();
            this.txtTsgIP = new System.Windows.Forms.TextBox();
            this.lblTSG = new System.Windows.Forms.Label();
            this.lblWanMask = new System.Windows.Forms.Label();
            this.lblTsgMask = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnAbout = new System.Windows.Forms.Button();
            this.cbDHCP = new System.Windows.Forms.CheckBox();
            this.lblDNS = new System.Windows.Forms.Label();
            this.lblStartIp = new System.Windows.Forms.Label();
            this.lblLastIp = new System.Windows.Forms.Label();
            this.lblPool = new System.Windows.Forms.Label();
            this.txtPool = new System.Windows.Forms.TextBox();
            this.btnCopy = new System.Windows.Forms.Button();
            this.lblVpnUser = new System.Windows.Forms.Label();
            this.lblVpnPass = new System.Windows.Forms.Label();
            this.txtVpnUser = new System.Windows.Forms.TextBox();
            this.txtVpnPass = new System.Windows.Forms.TextBox();
            this.lblPoolStart = new System.Windows.Forms.Label();
            this.lblPoolEnd = new System.Windows.Forms.Label();
            this.picTSG = new System.Windows.Forms.PictureBox();
            this.txtWanGW = new System.Windows.Forms.TextBox();
            this.lblWanGW = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblPwd = new System.Windows.Forms.Label();
            this.txtPwd = new System.Windows.Forms.TextBox();
            this.lblTeamPass = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picTSG)).BeginInit();
            this.SuspendLayout();
            // 
            // lblHostname
            // 
            this.lblHostname.AutoSize = true;
            this.lblHostname.Location = new System.Drawing.Point(12, 9);
            this.lblHostname.Name = "lblHostname";
            this.lblHostname.Size = new System.Drawing.Size(58, 13);
            this.lblHostname.TabIndex = 0;
            this.lblHostname.Text = "Hostname:";
            // 
            // lblWan
            // 
            this.lblWan.AutoSize = true;
            this.lblWan.Location = new System.Drawing.Point(12, 34);
            this.lblWan.Name = "lblWan";
            this.lblWan.Size = new System.Drawing.Size(47, 13);
            this.lblWan.TabIndex = 1;
            this.lblWan.Text = "WAN ip:";
            // 
            // txtHostname
            // 
            this.txtHostname.Location = new System.Drawing.Point(113, 6);
            this.txtHostname.Name = "txtHostname";
            this.txtHostname.Size = new System.Drawing.Size(324, 20);
            this.txtHostname.TabIndex = 2;
            this.txtHostname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NoSpace);
            // 
            // txtWanIP
            // 
            this.txtWanIP.Location = new System.Drawing.Point(113, 58);
            this.txtWanIP.MaxLength = 15;
            this.txtWanIP.Name = "txtWanIP";
            this.txtWanIP.Size = new System.Drawing.Size(217, 20);
            this.txtWanIP.TabIndex = 4;
            this.txtWanIP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PointsAndNumbers);
            // 
            // txtConfig
            // 
            this.txtConfig.Location = new System.Drawing.Point(23, 302);
            this.txtConfig.Multiline = true;
            this.txtConfig.Name = "txtConfig";
            this.txtConfig.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtConfig.Size = new System.Drawing.Size(587, 499);
            this.txtConfig.TabIndex = 18;
            // 
            // txtWanMask
            // 
            this.txtWanMask.Location = new System.Drawing.Point(388, 58);
            this.txtWanMask.MaxLength = 2;
            this.txtWanMask.Name = "txtWanMask";
            this.txtWanMask.Size = new System.Drawing.Size(49, 20);
            this.txtWanMask.TabIndex = 5;
            this.txtWanMask.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyNumbers);
            // 
            // txtDNS2
            // 
            this.txtDNS2.Location = new System.Drawing.Point(281, 155);
            this.txtDNS2.MaxLength = 15;
            this.txtDNS2.Name = "txtDNS2";
            this.txtDNS2.Size = new System.Drawing.Size(156, 20);
            this.txtDNS2.TabIndex = 10;
            this.txtDNS2.Text = "1.1.1.1";
            this.txtDNS2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PointsAndNumbers);
            // 
            // txtDNS1
            // 
            this.txtDNS1.Location = new System.Drawing.Point(113, 155);
            this.txtDNS1.MaxLength = 15;
            this.txtDNS1.Name = "txtDNS1";
            this.txtDNS1.Size = new System.Drawing.Size(156, 20);
            this.txtDNS1.TabIndex = 9;
            this.txtDNS1.Text = "8.8.8.8";
            this.txtDNS1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PointsAndNumbers);
            // 
            // txtTsgMask
            // 
            this.txtTsgMask.Location = new System.Drawing.Point(388, 112);
            this.txtTsgMask.MaxLength = 2;
            this.txtTsgMask.Name = "txtTsgMask";
            this.txtTsgMask.Size = new System.Drawing.Size(49, 20);
            this.txtTsgMask.TabIndex = 8;
            this.txtTsgMask.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyNumbers);
            // 
            // txtTsgIP
            // 
            this.txtTsgIP.Location = new System.Drawing.Point(113, 112);
            this.txtTsgIP.MaxLength = 15;
            this.txtTsgIP.Name = "txtTsgIP";
            this.txtTsgIP.Size = new System.Drawing.Size(217, 20);
            this.txtTsgIP.TabIndex = 7;
            this.txtTsgIP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PointsAndNumbers);
            // 
            // lblTSG
            // 
            this.lblTSG.AutoSize = true;
            this.lblTSG.Location = new System.Drawing.Point(12, 115);
            this.lblTSG.Name = "lblTSG";
            this.lblTSG.Size = new System.Drawing.Size(43, 13);
            this.lblTSG.TabIndex = 8;
            this.lblTSG.Text = "TSG ip:";
            // 
            // lblWanMask
            // 
            this.lblWanMask.AutoSize = true;
            this.lblWanMask.Location = new System.Drawing.Point(347, 61);
            this.lblWanMask.Name = "lblWanMask";
            this.lblWanMask.Size = new System.Drawing.Size(36, 13);
            this.lblWanMask.TabIndex = 11;
            this.lblWanMask.Text = "Mask:";
            // 
            // lblTsgMask
            // 
            this.lblTsgMask.AutoSize = true;
            this.lblTsgMask.Location = new System.Drawing.Point(347, 115);
            this.lblTsgMask.Name = "lblTsgMask";
            this.lblTsgMask.Size = new System.Drawing.Size(36, 13);
            this.lblTsgMask.TabIndex = 12;
            this.lblTsgMask.Text = "Mask:";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(113, 273);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(75, 23);
            this.btnGenerate.TabIndex = 15;
            this.btnGenerate.Text = "&Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(265, 273);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 16;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.Location = new System.Drawing.Point(413, 273);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(75, 23);
            this.btnAbout.TabIndex = 17;
            this.btnAbout.Text = "About";
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // cbDHCP
            // 
            this.cbDHCP.AutoSize = true;
            this.cbDHCP.Checked = true;
            this.cbDHCP.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDHCP.Location = new System.Drawing.Point(113, 33);
            this.cbDHCP.Name = "cbDHCP";
            this.cbDHCP.Size = new System.Drawing.Size(56, 17);
            this.cbDHCP.TabIndex = 3;
            this.cbDHCP.Text = "DHCP";
            this.cbDHCP.UseVisualStyleBackColor = true;
            this.cbDHCP.CheckedChanged += new System.EventHandler(this.cbDHCP_CheckedChanged);
            // 
            // lblDNS
            // 
            this.lblDNS.AutoSize = true;
            this.lblDNS.Location = new System.Drawing.Point(12, 158);
            this.lblDNS.Name = "lblDNS";
            this.lblDNS.Size = new System.Drawing.Size(72, 13);
            this.lblDNS.TabIndex = 17;
            this.lblDNS.Text = "DNS Servers:";
            // 
            // lblStartIp
            // 
            this.lblStartIp.AutoSize = true;
            this.lblStartIp.Location = new System.Drawing.Point(110, 135);
            this.lblStartIp.Name = "lblStartIp";
            this.lblStartIp.Size = new System.Drawing.Size(47, 13);
            this.lblStartIp.TabIndex = 18;
            this.lblStartIp.Text = "Network";
            // 
            // lblLastIp
            // 
            this.lblLastIp.AutoSize = true;
            this.lblLastIp.Location = new System.Drawing.Point(295, 135);
            this.lblLastIp.Name = "lblLastIp";
            this.lblLastIp.Size = new System.Drawing.Size(55, 13);
            this.lblLastIp.TabIndex = 19;
            this.lblLastIp.Text = "Broadcast";
            // 
            // lblPool
            // 
            this.lblPool.AutoSize = true;
            this.lblPool.Location = new System.Drawing.Point(12, 188);
            this.lblPool.Name = "lblPool";
            this.lblPool.Size = new System.Drawing.Size(64, 13);
            this.lblPool.TabIndex = 20;
            this.lblPool.Text = "DHCP Pool:";
            // 
            // txtPool
            // 
            this.txtPool.Location = new System.Drawing.Point(113, 185);
            this.txtPool.Name = "txtPool";
            this.txtPool.Size = new System.Drawing.Size(63, 20);
            this.txtPool.TabIndex = 11;
            this.txtPool.Text = "10";
            this.txtPool.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyNumbers);
            // 
            // btnCopy
            // 
            this.btnCopy.Location = new System.Drawing.Point(23, 812);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(165, 23);
            this.btnCopy.TabIndex = 19;
            this.btnCopy.Text = "&Copy To Clipboard";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // lblVpnUser
            // 
            this.lblVpnUser.AutoSize = true;
            this.lblVpnUser.Location = new System.Drawing.Point(12, 216);
            this.lblVpnUser.Name = "lblVpnUser";
            this.lblVpnUser.Size = new System.Drawing.Size(57, 13);
            this.lblVpnUser.TabIndex = 23;
            this.lblVpnUser.Text = "VPN User:";
            // 
            // lblVpnPass
            // 
            this.lblVpnPass.AutoSize = true;
            this.lblVpnPass.Location = new System.Drawing.Point(317, 216);
            this.lblVpnPass.Name = "lblVpnPass";
            this.lblVpnPass.Size = new System.Drawing.Size(81, 13);
            this.lblVpnPass.TabIndex = 24;
            this.lblVpnPass.Text = "VPN Password:";
            // 
            // txtVpnUser
            // 
            this.txtVpnUser.Location = new System.Drawing.Point(113, 213);
            this.txtVpnUser.Name = "txtVpnUser";
            this.txtVpnUser.Size = new System.Drawing.Size(198, 20);
            this.txtVpnUser.TabIndex = 12;
            this.txtVpnUser.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NoSpace);
            // 
            // txtVpnPass
            // 
            this.txtVpnPass.Location = new System.Drawing.Point(404, 213);
            this.txtVpnPass.Name = "txtVpnPass";
            this.txtVpnPass.Size = new System.Drawing.Size(198, 20);
            this.txtVpnPass.TabIndex = 13;
            this.txtVpnPass.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NoSpace);
            // 
            // lblPoolStart
            // 
            this.lblPoolStart.AutoSize = true;
            this.lblPoolStart.Location = new System.Drawing.Point(197, 188);
            this.lblPoolStart.Name = "lblPoolStart";
            this.lblPoolStart.Size = new System.Drawing.Size(56, 13);
            this.lblPoolStart.TabIndex = 27;
            this.lblPoolStart.Text = "Pool Start:";
            // 
            // lblPoolEnd
            // 
            this.lblPoolEnd.AutoSize = true;
            this.lblPoolEnd.Location = new System.Drawing.Point(363, 188);
            this.lblPoolEnd.Name = "lblPoolEnd";
            this.lblPoolEnd.Size = new System.Drawing.Size(53, 13);
            this.lblPoolEnd.TabIndex = 28;
            this.lblPoolEnd.Text = "Pool End:";
            // 
            // picTSG
            // 
            this.picTSG.Image = global::DefconRB.Properties.Resources._0;
            this.picTSG.ImageLocation = "";
            this.picTSG.Location = new System.Drawing.Point(471, 25);
            this.picTSG.Name = "picTSG";
            this.picTSG.Size = new System.Drawing.Size(131, 129);
            this.picTSG.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picTSG.TabIndex = 30;
            this.picTSG.TabStop = false;
            // 
            // txtWanGW
            // 
            this.txtWanGW.Location = new System.Drawing.Point(220, 84);
            this.txtWanGW.MaxLength = 15;
            this.txtWanGW.Name = "txtWanGW";
            this.txtWanGW.Size = new System.Drawing.Size(217, 20);
            this.txtWanGW.TabIndex = 6;
            // 
            // lblWanGW
            // 
            this.lblWanGW.AutoSize = true;
            this.lblWanGW.Location = new System.Drawing.Point(110, 87);
            this.lblWanGW.Name = "lblWanGW";
            this.lblWanGW.Size = new System.Drawing.Size(52, 13);
            this.lblWanGW.TabIndex = 32;
            this.lblWanGW.Text = "Gateway:";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(445, 812);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(165, 23);
            this.btnSave.TabIndex = 20;
            this.btnSave.Text = "&Save To File";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblPwd
            // 
            this.lblPwd.AutoSize = true;
            this.lblPwd.Location = new System.Drawing.Point(12, 245);
            this.lblPwd.Name = "lblPwd";
            this.lblPwd.Size = new System.Drawing.Size(56, 13);
            this.lblPwd.TabIndex = 34;
            this.lblPwd.Text = "Password:";
            // 
            // txtPwd
            // 
            this.txtPwd.Location = new System.Drawing.Point(113, 242);
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.Size = new System.Drawing.Size(198, 20);
            this.txtPwd.TabIndex = 14;
            this.txtPwd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NoSpace);
            // 
            // lblTeamPass
            // 
            this.lblTeamPass.AutoSize = true;
            this.lblTeamPass.ForeColor = System.Drawing.Color.Red;
            this.lblTeamPass.Location = new System.Drawing.Point(317, 245);
            this.lblTeamPass.Name = "lblTeamPass";
            this.lblTeamPass.Size = new System.Drawing.Size(177, 13);
            this.lblTeamPass.TabIndex = 36;
            this.lblTeamPass.Text = "Please add credentials to teampass!";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 847);
            this.Controls.Add(this.lblTeamPass);
            this.Controls.Add(this.txtPwd);
            this.Controls.Add(this.lblPwd);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblWanGW);
            this.Controls.Add(this.txtWanGW);
            this.Controls.Add(this.picTSG);
            this.Controls.Add(this.lblPoolEnd);
            this.Controls.Add(this.lblPoolStart);
            this.Controls.Add(this.txtVpnPass);
            this.Controls.Add(this.txtVpnUser);
            this.Controls.Add(this.lblVpnPass);
            this.Controls.Add(this.lblVpnUser);
            this.Controls.Add(this.btnCopy);
            this.Controls.Add(this.txtPool);
            this.Controls.Add(this.lblPool);
            this.Controls.Add(this.lblLastIp);
            this.Controls.Add(this.lblStartIp);
            this.Controls.Add(this.lblDNS);
            this.Controls.Add(this.cbDHCP);
            this.Controls.Add(this.btnAbout);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.lblTsgMask);
            this.Controls.Add(this.lblWanMask);
            this.Controls.Add(this.txtTsgMask);
            this.Controls.Add(this.txtTsgIP);
            this.Controls.Add(this.lblTSG);
            this.Controls.Add(this.txtDNS1);
            this.Controls.Add(this.txtDNS2);
            this.Controls.Add(this.txtWanMask);
            this.Controls.Add(this.txtConfig);
            this.Controls.Add(this.txtWanIP);
            this.Controls.Add(this.txtHostname);
            this.Controls.Add(this.lblWan);
            this.Controls.Add(this.lblHostname);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DefconRB";
            ((System.ComponentModel.ISupportInitialize)(this.picTSG)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHostname;
        private System.Windows.Forms.Label lblWan;
        private System.Windows.Forms.TextBox txtHostname;
        private System.Windows.Forms.TextBox txtWanIP;
        private System.Windows.Forms.TextBox txtConfig;
        private System.Windows.Forms.TextBox txtWanMask;
        private System.Windows.Forms.TextBox txtDNS2;
        private System.Windows.Forms.TextBox txtDNS1;
        private System.Windows.Forms.TextBox txtTsgMask;
        private System.Windows.Forms.TextBox txtTsgIP;
        private System.Windows.Forms.Label lblTSG;
        private System.Windows.Forms.Label lblWanMask;
        private System.Windows.Forms.Label lblTsgMask;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.CheckBox cbDHCP;
        private System.Windows.Forms.Label lblDNS;
        private System.Windows.Forms.Label lblStartIp;
        private System.Windows.Forms.Label lblLastIp;
        private System.Windows.Forms.Label lblPool;
        private System.Windows.Forms.TextBox txtPool;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.Label lblVpnUser;
        private System.Windows.Forms.Label lblVpnPass;
        private System.Windows.Forms.TextBox txtVpnUser;
        private System.Windows.Forms.TextBox txtVpnPass;
        private System.Windows.Forms.Label lblPoolStart;
        private System.Windows.Forms.Label lblPoolEnd;
        private System.Windows.Forms.PictureBox picTSG;
        private System.Windows.Forms.TextBox txtWanGW;
        private System.Windows.Forms.Label lblWanGW;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblPwd;
        private System.Windows.Forms.TextBox txtPwd;
        private System.Windows.Forms.Label lblTeamPass;
    }
}

