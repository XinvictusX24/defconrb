﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace DefconRB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ResetForm();
        }

        // different variables needed per config
        public static string Hostname;
        public static string TSGip;
        public static string WanIP;
        public static string DNS;
        public static string Gateway;
        public static string WanGateway;
        public static string NetworkAddress;
        public static string BroadcastAddress;
        public static string TSGmask;
        public static string PoolStart;
        public static string PoolEnd;
        public static string VPNpass;
        public static string VPNuser;
        public static string Password;

        static long ToInt(string addr)
        {            
            return (long)(uint)IPAddress.NetworkToHostOrder((int)IPAddress.Parse(addr).Address);
        }

        static string ToAddr(long address)
        {
            return IPAddress.Parse(address.ToString()).ToString();           
        }


        private void GenerateConfig ()
        {            
            Hostname = txtHostname.Text;
            TSGip = txtTsgIP.Text + "/" + txtTsgMask.Text;
            Gateway = txtTsgIP.Text;
            TSGmask = txtTsgMask.Text;
            VPNuser = txtVpnUser.Text;
            VPNpass = txtVpnPass.Text;
            Password = txtPwd.Text;
            

            if (cbDHCP.Checked){WanIP = "/ip dhcp-client\r\n add disabled=no interface=bridge-WAN \r\n";}
            else{WanIP = "add address=" + txtWanIP.Text +"/"+txtWanMask.Text+" interface=bridge-WAN \r\n";}
            
            if (string.IsNullOrWhiteSpace(txtDNS1.Text)) {DNS=txtDNS2.Text; }
            else if (string.IsNullOrWhiteSpace(txtDNS2.Text)) { DNS = txtDNS1.Text; }
            else { DNS = txtDNS1.Text + "," + txtDNS2.Text; }

            //standard conf sander
            string Config =
                "/interface ovpn-client\r\n" +
                $"add cipher=aes256 connect-to=vpn.thesafegroop.com name=ovpn-out-DC1 password={VPNpass} user={VPNuser}\r\n" +
                "/interface bridge\r\n" +
                "add name=bridge-TSG protocol-mode=none\r\n" +
                "add name=bridge-WAN protocol-mode=none\r\n" +
                "/interface wireless\r\n" +
                "set [find default-name=wlan1] ssid=MikroTik\r\n" +
                "/interface list\r\n" +
                "add name=Trusted\r\n" +
                "add name=Untrusted\r\n" +
                "add name=Internal\r\n" +
                "add name=external\r\n" +
                "add name=wan\r\n" +
                "/interface wireless security-profiles\r\n" +
                "set [find default=yes] supplicant-identity=MikroTik\r\n" +
                "/ip pool\r\n" +
                $"add name=dhcp_pool0 ranges={PoolStart}-{PoolEnd}\r\n" +
                "/ip dhcp-server\r\n" +
                "add address-pool=dhcp_pool0 disabled=no interface=bridge-TSG name=dhcp1\r\n" +
                "/interface bridge port\r\n" +
                "add bridge=bridge-TSG interface=ether2\r\n" +
                "add bridge=bridge-WAN interface=ether1\r\n" +
                "/interface list member\r\n" +
                "add interface=bridge-TSG list=Trusted\r\n" +
                "add interface=ovpn-out-DC1 list=Trusted\r\n" +
                "add interface=bridge-TSG list=Internal\r\n" +
                "add interface=bridge-WAN list=external\r\n" +
                "add interface=bridge-WAN list=wan\r\n" +
                "/ip address\r\n" +
                $"add address={TSGip} interface=bridge-TSG\r\n" +
                WanIP +
                "/ip dhcp-server network\r\n" +
                $"add address={NetworkAddress}/{TSGmask} dns-server={Gateway},8.8.8.8 gateway={Gateway}\r\n" +
                $"/ip dns set allow-remote-requests=yes servers={DNS}\r\n" +
                "/ip firewall filter\r\n" +
                "add action=accept chain=input connection-state=established,related\r\n" +
                "add action=accept chain=input in-interface=bridge-TSG\r\n" +
                "add action=accept chain=input in-interface=ovpn-out-DC1\r\n" +
                "add action=drop chain=input\r\n" +
                "add action=accept chain=forward connection-state=established,related\r\n" +
                "add action=accept chain=forward in-interface=bridge-TSG\r\n" +
                "add action=accept chain=forward in-interface=ovpn-out-DC1\r\n" +
                "add action=drop chain=forward\r\n" +
                "/ip firewall nat\r\n" +
                "add action=masquerade chain=srcnat out-interface=bridge-WAN\r\n" +
                "/ip firewall service-port\r\n" +
                "set h323 disabled=yes\r\n" +
                "set sip disabled=yes\r\n" +
                "/ip route\r\n" +
                "add distance=1 dst-address=10.148.0.0/14 gateway=ovpn-out-DC1\r\n" +
                "/system clock\r\n" +
                "set time-zone-name=Europe/Brussels\r\n" +
                "/system identity\r\n" +
                $"set name={Hostname}\r\n" +
                "/system ntp client\r\n" +
                "set enabled=yes primary-ntp=193.190.147.153 secondary-ntp=162.159.200.1\r\n" +
                "/tool mac-server\r\n" +
                "set allowed-interface-list=Trusted\r\n" +
                "/tool mac-server mac-winbox\r\n" +
                "set allowed-interface-list=Trusted\r\n"+
                $"/user set admin password={Password}";

            txtConfig.Text = Config;
        }

        private void ResetForm()
        {
            //reset form
            txtConfig.Clear();
            txtHostname.Clear();
            txtTsgIP.Clear();
            txtTsgMask.Clear();
            txtWanIP.Clear();
            txtWanMask.Clear();
            txtDNS1.Text = "8.8.8.8";
            txtDNS2.Text = "1.1.1.1";
            lblStartIp.Text = "";
            lblLastIp.Text = "";
            cbDHCP.Checked = true;
            txtWanIP.Hide();
            txtWanMask.Hide();
            lblWanMask.Hide();
            txtWanGW.Hide();
            lblWanGW.Hide();
            btnCopy.Hide();
            lblPoolStart.Text="";
            lblPoolEnd.Text="";
            btnSave.Hide();
            lblTeamPass.Hide();
        }
        private void btnGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                //check for wrong input
                if (String.IsNullOrWhiteSpace(txtHostname.Text)) {MessageBox.Show("Please enter a Hostname", "Hostname Error");}
                else if (IPAddress.TryParse(txtWanIP.Text, out IPAddress wanIP) == false && cbDHCP.Checked == false) { MessageBox.Show("Please enter a valid IP address", "Invalid WAN IP"); }
                else if (txtWanIP.Text.Contains(".") == false && cbDHCP.Checked == false) { MessageBox.Show("Please enter a valid IP address", "Invalid WAN IP"); }
                else if (IPAddress.TryParse(txtTsgIP.Text, out IPAddress TSGIP) == false || txtTsgIP.Text.Contains(".") ==false) { MessageBox.Show("Please enter a valid IP address", "Invalid TSG IP"); }
                else if (int.Parse(txtTsgMask.Text) > 32 || (txtWanMask.Text == null && int.Parse(txtWanMask.Text) > 32)) { MessageBox.Show("Mask can never be greater than 32", "Wrong NetMask"); }
                else if (String.IsNullOrWhiteSpace(txtTsgIP.Text) || String.IsNullOrWhiteSpace(txtTsgMask.Text)){MessageBox.Show("Please enter a valid IP for Bridge-TSG", "Bridge-TSG Error");}
                else if (!cbDHCP.Checked && (String.IsNullOrWhiteSpace(txtWanIP.Text) || String.IsNullOrWhiteSpace(txtWanMask.Text))) {MessageBox.Show("Please enter a valid IP for Bridge-WAN or check DHCP option", "Bridge-WAN Error"); }
                else if (!cbDHCP.Checked && (String.IsNullOrWhiteSpace(txtWanGW.Text) || String.IsNullOrWhiteSpace(txtWanMask.Text))) { MessageBox.Show("Please enter a Gateway for Bridge-WAN or check DHCP option", "Bridge-WAN Error"); }
                else if (string.IsNullOrWhiteSpace(txtDNS1.Text)&& string.IsNullOrWhiteSpace(txtDNS2.Text)) {MessageBox.Show("Please enter at least 1 DNS server", "DNS Error"); }
                else if (string.IsNullOrWhiteSpace(txtVpnPass.Text) || string.IsNullOrWhiteSpace(txtVpnUser.Text)) { MessageBox.Show("Please enter vpn Credentials", "VPN Error"); }
                else if (string.IsNullOrWhiteSpace(txtPool.Text)) { MessageBox.Show("Please enter a number of pool adresses", "DHCP pool Error"); }
                else
                {
                    IPcalc(); 
                    GenerateConfig(); 
                    btnCopy.Show();
                    btnSave.Show();
                    lblTeamPass.Show();
                }               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //ResetForm();
            }           
        }
        private void btnAbout_Click(object sender, EventArgs e)
        {
            txtConfig.Text = "Made by Sander Vanvuchelen and Kurt Vanoppen \r\nVersion 0.02\r\n"+
            "tested on firmware:\r\n"+
            "4.??.??";
            btnCopy.Hide();
            btnSave.Hide();
        }

        
        private void btnClear_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void IPcalc()
        {
            //variables for calculating pool
            long PoolLong;


            IPAddress ip = IPAddress.Parse(txtTsgIP.Text);
            int bits = int.Parse(txtTsgMask.Text);

            uint mask = ~(uint.MaxValue >> bits);

            // Convert the IP address to bytes.
            byte[] ipBytes = ip.GetAddressBytes();

            // BitConverter gives bytes in opposite order to GetAddressBytes().
            byte[] maskBytes = BitConverter.GetBytes(mask).Reverse().ToArray();

            byte[] startIPBytes = new byte[ipBytes.Length];
            byte[] endIPBytes = new byte[ipBytes.Length];

            // Calculate the bytes of the start and end IP addresses.
            for (int i = 0; i < ipBytes.Length; i++)
            {
                startIPBytes[i] = (byte)(ipBytes[i] & maskBytes[i]);
                endIPBytes[i] = (byte)(ipBytes[i] | ~maskBytes[i]);
            }

            // Convert the bytes to IP addresses.
            IPAddress startIP = new IPAddress(startIPBytes);
            IPAddress endIP = new IPAddress(endIPBytes);

            NetworkAddress = startIP.ToString();
            BroadcastAddress = endIP.ToString();

            //print network and broadcast address
            lblStartIp.Text = "Network: " + NetworkAddress;
            lblLastIp.Text = "Broadcast: " + BroadcastAddress;

            //DHCP pool start address
            PoolLong = (ToInt(BroadcastAddress) - 1 - int.Parse(txtPool.Text));
            PoolStart = ToAddr(PoolLong);
            lblPoolStart.Text = "Start IP: " + PoolStart;

            //DCHC pool end address
            PoolLong = (ToInt(BroadcastAddress) - 1);
            PoolEnd = ToAddr(PoolLong);
            lblPoolEnd.Text = "End IP: " + PoolEnd;


        }

        private void cbDHCP_CheckedChanged(object sender, EventArgs e)
        {
            if (cbDHCP.Checked)
            {
                txtWanIP.Hide();
                txtWanMask.Hide();
                lblWanMask.Hide();
                txtWanGW.Hide();
                lblWanGW.Hide();
            }
            else
            {
                txtWanIP.Show();
                txtWanMask.Show();
                lblWanMask.Show();
                txtWanGW.Show();
                lblWanGW.Show();
            }
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txtConfig.Text);
        }
        private void OnlyNumbers(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        private void PointsAndNumbers(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }
        private void NoSpace(object sender, KeyPressEventArgs e)
        {
            e.Handled = (e.KeyChar == (char)Keys.Space);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Title = "Save Config File";
            saveFile.FileName = "NewConfig";
            saveFile.DefaultExt = ".txt";
            saveFile.ShowDialog();

            //If the file name is not an empty string open it for saving. && saveFile.ShowDialog() == DialogResult.OK
            if (saveFile.FileName != "" )
            {
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(saveFile.FileName))
                {
                    sw.Write(txtConfig.Text);
                    sw.Flush(); 
                    sw.Close();
                }
            }
        }
    }
}